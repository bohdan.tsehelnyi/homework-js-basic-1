// ============THEORY=====================

/* 1. Змінні в JS оголошуються за допомогою ключових слів:

- var (є застарілим варіантом і використовується лише для підтримки старих сайтів);
- let (оголошує змінну яку можна переприсвоїти)
- const (оголошує змінну яку не можна переприсвоїти)

До змінної обов'язково добавляємо ідентифікатор (ім'я).

Ім'я має писатися з визначеним синтаксисом, а також є певні зарезервовані слова, які не можна використовувати як назву змінних.

Приклад (допускається):
let name;
let _name;
let $name;

Приклад (не допускається):
let 2name; - перший символ має бути літера або знак
let 123; - іменувати перемінну лише цифрами не допускається
let for; - for є зарезервованим словом



2. String (рядок) є примітивним типом данних в JS який має безліч символів, які ОБОВ'ЯЗКОВО пишуться в одинарних чи подвійних лапках.

Створюється рядок за допомогою:

- Написання значення як рядка:
let str = "Hello World!!!";

- У випадку додавання рядків:
let attached = "Hello " + "World!";   -Hello World!

-У випадку склеювання (+) рядка з іншим значенням:
let attached = 22 + "22";   - 2222 - string

- У випадку перетворення типу:
let attached = String(22); 



3. Перевірка типу данних змінної в JS проводиться за допомогою оператора "typeof"

Приклад:
let num = 10;
console.log(typeof(num));

4. 1 + "1" = 11;
При використанні оператора + між двома операндами з типом 'Number' та типом 'String' відбувається конкатенація (склеювання) числа і рядка де число переходить в рядок.
Але така маніпуляція спрацює лише з оператором +. З іншими операторами (/, *, -, %) результатом буде тип Number.
*/


// ==================PRACTICE========================

// 1.
"use strict"

let num = 10;
console.log(typeof num);
console.log(num);

// 2.
const name = "Bohdan";
const lastName = "Tsehelnyi";
console.log(`My name is ${name} and my last name is ${lastName}`);

// 3.
const numTwo = 100;
const strNum = "I\'ve " + numTwo + " apples";
console.log(strNum);
console.log(`I haven\'t ${numTwo} apples`);
